#!/usr/bin/env bash
# $1 ... install prefix -- defaults to /usr/local

PREFIX="${1:-/usr/local}"
USETAR=no

# tarball is kinda out of date and not compiling
#BASEURL="http://svn.macosforge.org/repository/odcctools/release"
#ODCCTOOLS=odcctools-20090808.tar.bz2
#ODCCTOOLS_MD5SUM=cb8ce536e204fc2399fc27079012c37b


# svn version
BASEURL="http://svn.macosforge.org/repository/odcctools/trunk/"
ODCCTOOLS=odcctools

function die() {
	echo "ERROR: ${1}"
	exit 1
}

# tarball fetch
if [[ ${USETAR} != no ]]; then
	if [[ -e ${ODCCTOOLS} ]]; then
		MD5LINE="${ODCCTOOLS_MD5SUM}  ${ODCCTOOLS}"
		if ! echo "${MD5LINE}" | md5sum -c --quiet -; then
			echo "Re-fetching ${ODCCTOOLS} due to checksum error"
			rm -f ${ODCCTOOLS}
		else
			echo "Using pre-downloaded ${ODCCTOOLS}"
			NOFETCH=yes
		fi
	fi

	if [[ ${NOFETCH} != yes ]]; then
		wget "${BASEURL}/${ODCCTOOLS}"
	fi

	tar jxf ${ODCCTOOLS} || die "Unpacking ${ODCCTOOLS} failed"
	S=${ODCCTOOLS/.tar.bz2}
# subversion fetch
else
	if [[ -e ${ODCCTOOLS} ]]; then
		pushd ${ODCCTOOLS}
		svn up
		popd
	else
		svn co "${BASEURL}" ${ODCCTOOLS}
	fi

	pushd ${ODCCTOOLS}
	./extract.sh || die "Unpacking with ./extract.sh failed"
	popd
	S=${ODCCTOOLS}/odcctools
fi




# compilation
pushd ${S} || die "pushd failed"
./configure --target=i386-apple-darwin9 --prefix="${PREFIX}" || die "configure failed"
make -C libstuff || die "make libstuff failed"
make -C misc lipo || die "make lipo failed"
mkdir -p "${PREFIX}"/bin || die "mkdir install dir failed"
cp misc/lipo "${PREFIX}"/bin/ || die "copying lipo failed"
popd || die "popd failed"
