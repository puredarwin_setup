export EPREFIX="$HOME/Gentoo" || exit
#export EPREFIX="/Volumes/Data/Gentoo" || exit
export PATH="$EPREFIX/usr/bin:$EPREFIX/bin:$EPREFIX/tmp/usr/bin:$EPREFIX/tmp/bin:$PATH" || exit
#export MAKEOPTS="-j5"
#export PORTAGE_TMPDIR="/Volumes/Temp/portage"

chmod 755 bootstrap-prefix.sh || exit
./bootstrap-prefix.sh $EPREFIX tree || exit
./bootstrap-prefix.sh $EPREFIX/tmp make || exit
./bootstrap-prefix.sh $EPREFIX/tmp wget || exit
./bootstrap-prefix.sh $EPREFIX/tmp sed || exit
# python needs patch python-2.6.4-puredarwin-_scproxy.patch
./bootstrap-prefix.sh $EPREFIX/tmp python || exit
./bootstrap-prefix.sh $EPREFIX/tmp coreutils6 || exit
./bootstrap-prefix.sh $EPREFIX/tmp findutils || exit
./bootstrap-prefix.sh $EPREFIX/tmp tar15 || exit
./bootstrap-prefix.sh $EPREFIX/tmp patch9 || exit
./bootstrap-prefix.sh $EPREFIX/tmp grep || exit
./bootstrap-prefix.sh $EPREFIX/tmp gawk || exit
# bash needs yacc so we need bison
./bootstrap-prefix.sh $EPREFIX/tmp bison || exit
./bootstrap-prefix.sh $EPREFIX/tmp bash || exit
./bootstrap-prefix.sh $EPREFIX portage || exit



hash -r || exit


emerge --oneshot --nodeps bash || exit
emerge --oneshot pax-utils || exit
emerge --oneshot --nodeps wget || exit


emerge --oneshot --nodeps baselayout-prefix || exit
emerge --oneshot --nodeps xz-utils || exit
emerge --oneshot --nodeps m4 || exit
emerge --oneshot --nodeps flex || exit
emerge --oneshot --nodeps bison || exit
emerge --oneshot --nodeps binutils-config || exit
emerge --oneshot --nodeps binutils-apple || exit
# needed for autoconf
emerge --oneshot --nodeps perl || exit
# both needed for automake
emerge --oneshot --nodeps autoconf || exit
emerge --oneshot --nodeps autoconf-wrapper || exit
# needed for gcc-apple
emerge --oneshot --nodeps help2man || exit
emerge --oneshot --nodeps texinfo || exit
emerge --oneshot --nodeps automake || exit
emerge --oneshot --nodeps automake-wrapper || exit
emerge --oneshot --nodeps gcc-config || exit
emerge --oneshot --nodeps "<gcc-apple-4.2.1_p5646" || exit


emerge --oneshot coreutils || exit
emerge --oneshot findutils || exit
emerge --oneshot tar || exit
emerge --oneshot grep || exit
emerge --oneshot patch || exit
emerge --oneshot gawk || exit
emerge --oneshot make || exit
emerge --oneshot --nodeps file || exit
emerge --oneshot --nodeps eselect || exit
emerge --oneshot pax-utils || exit


# needs fixed python ebuild
env USE="-aqua" env FEATURES="-collision-protect" emerge --oneshot portage || exit


rm -Rf $EPREFIX/tmp/* || exit
hash -r || exit


# after syncing we need to restore the patched python ebuild :/
emerge --sync || exit


emerge -u system || exit


echo "please set up your make.conf and run emerge -e system" || exit
